// Compiled using marko@4.11.2 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_componentType = "/app$1.0.0/views/Home/welcome/index.marko",
    marko_component = require("./component"),
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_loadTemplate = require("marko/src/runtime/helper-loadTemplate"),
    nav_template = marko_loadTemplate(require.resolve("../../components/nav")),
    marko_helpers = require("marko/src/runtime/html/helpers"),
    marko_loadTag = marko_helpers.t,
    nav_tag = marko_loadTag(nav_template),
    marko_forEach = marko_helpers.f,
    marko_escapeXml = marko_helpers.x,
    layout_template = marko_loadTemplate(require.resolve("../../components/layout")),
    layout_tag = marko_loadTag(layout_template);

function render(input, out, __component, component, state) {
  var data = input;

  layout_tag({
      renderBody: function renderBody(out) {
        nav_tag({
            a: input.posts,
            name: "asda"
          }, out, __component, "1");

        out.w("<h1>Hello Marko</h1>");

        marko_forEach(input.posts, function(post) {
          out.w("<div style=\"background:red\"><p>" +
            marko_escapeXml(post.title) +
            "</p></div>");
        });

        out.w("<a href=\"/test/33\">testtt</a>");
      }
    }, out, __component, "0");
}

marko_template._ = marko_renderer(render, {
    ___type: marko_componentType
  }, marko_component);

marko_template.Component = marko_defineComponent(marko_component, marko_template._);

marko_template.meta = {
    id: "/app$1.0.0/views/Home/welcome/index.marko",
    component: "./",
    tags: [
      "../../components/nav",
      "../../components/layout"
    ]
  };
