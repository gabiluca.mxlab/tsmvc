import { Request, Response } from 'back-js';
import { Back } from 'back-js';

import http = require('http');
import path = require('path');
import fs = require('fs');
import bodyParser = require('body-parser');

require('marko/node-require'); // Allow Node.js to require and load `.marko` files
var markoExpress = require('marko/express');

/** import controllers so they can be analysed*/
// require('./controllers/productController');
// require('./controllers/homeController');

var templates = {};
var controllers = fs.readdirSync('./controllers');
controllers.forEach((name) => {
	if (name.lastIndexOf('.js') < 0) return;
	require('./controllers/' + name);
});

let express = Back.express;
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(markoExpress()); //enable res.marko(template, data)

export function template(action, controller) {
	controller = controller.substring(0, controller.indexOf('Controller'));
	return templates[action] || (templates[action] = require(`./views/${controller}/${action}`));
}
/**
 * prepare express routes
 */
Back.prepare(app);

var port = process.env.PORT || '3000';
app.set('port', port);
var server = http.createServer(app);
server.listen(port);
