"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var back_js_1 = require("back-js");
var productService_1 = require("../services/productService");
var ProductController = /** @class */ (function () {
    function ProductController(productService) {
        this.productService = productService;
    }
    ProductController.prototype.getProduct = function (req, res) {
        return this.productService.getProduct();
    };
    ProductController.prototype.addProduct = function (product) {
        console.log(product);
    };
    __decorate([
        back_js_1.Get("/"),
        back_js_1.ResponseBody,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [back_js_1.Request, back_js_1.Response]),
        __metadata("design:returntype", Promise)
    ], ProductController.prototype, "getProduct", null);
    __decorate([
        back_js_1.Post("/"),
        __param(0, back_js_1.RequestBody),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ProductController.prototype, "addProduct", null);
    ProductController = __decorate([
        back_js_1.Controller,
        back_js_1.Route("/product"),
        __metadata("design:paramtypes", [productService_1.ProductService])
    ], ProductController);
    return ProductController;
}());
