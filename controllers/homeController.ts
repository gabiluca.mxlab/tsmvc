import { BaseController } from './BaseController';
import { Controller, Get, Route, Request, Response } from 'back-js';
import axios = require('axios');

@Controller
@Route('/')
class HomeController extends BaseController {
	constructor() {
		super();
	}

	@Get('/')
	welcome(q: Request, r: Response) {
		axios.default.get('https://jsonplaceholder.typicode.com/posts').then((result) => {
			this.view(q, r, { posts: result.data });
		});
	}

	@Get('/test/:id')
	test(q: Request, r: Response, id: number) {
		console.log(id);

		this.view(q, r, { name: 'Frank' + id });
	}
}
