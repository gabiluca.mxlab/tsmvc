
import {Controller ,Get ,Post,Route ,Request ,Response, RequestBody, ResponseBody } from "back-js";

import {ProductService} from "../services/productService";
import {Product}  from "../model/product"; 


@Controller
@Route("/product")
class ProductController{

    constructor(
        private productService : ProductService
    ){}

    @Get("/")
    @ResponseBody
    getProduct(req : Request ,res : Response ) : Promise<Product> {
        return this.productService.getProduct();
    }

    @Post("/")
    addProduct(@RequestBody product){
        console.log(product);
    }

} 