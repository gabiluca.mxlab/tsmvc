import { template } from './../app';
import { Request, Response } from 'back-js';

export class BaseController {
	view(q: Request, r: Response, data, viewPath = null) {
		var qq: any = q,
			rr: any = r;

		var view = template(qq.action, qq.controller);
		if (viewPath) view = viewPath;
		rr.marko(view, data);
	}
}
