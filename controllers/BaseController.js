"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = require("./../app");
var BaseController = /** @class */ (function () {
    function BaseController() {
    }
    BaseController.prototype.view = function (q, r, data, viewPath) {
        if (viewPath === void 0) { viewPath = null; }
        var qq = q, rr = r;
        var view = app_1.template(qq.action, qq.controller);
        if (viewPath)
            view = viewPath;
        rr.marko(view, data);
    };
    return BaseController;
}());
exports.BaseController = BaseController;
