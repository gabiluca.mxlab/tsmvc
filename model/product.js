"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Product = /** @class */ (function () {
    function Product(id, label, price) {
        this.id = id;
        this.label = label;
        this.price = price;
    }
    return Product;
}());
exports.Product = Product;
