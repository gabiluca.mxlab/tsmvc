"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var back_js_1 = require("back-js");
var http = require("http");
var path = require("path");
var fs = require("fs");
var bodyParser = require("body-parser");
require('marko/node-require'); // Allow Node.js to require and load `.marko` files
var markoExpress = require('marko/express');
/** import controllers so they can be analysed*/
// require('./controllers/productController');
// require('./controllers/homeController');
var templates = {};
var controllers = fs.readdirSync('./controllers');
controllers.forEach(function (name) {
    if (name.lastIndexOf('.js') < 0)
        return;
    require('./controllers/' + name);
});
var express = back_js_1.Back.express;
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(markoExpress()); //enable res.marko(template, data)
function template(action, controller) {
    controller = controller.substring(0, controller.indexOf('Controller'));
    return templates[action] || (templates[action] = require("./views/" + controller + "/" + action));
}
exports.template = template;
/**
 * prepare express routes
 */
back_js_1.Back.prepare(app);
var port = process.env.PORT || '3000';
app.set('port', port);
var server = http.createServer(app);
server.listen(port);
